<?php
declare(strict_types=1);

namespace UCRM\Routing\Middlewares;

/**
 * A class to handle all aspects of a single Route.
 *
 * @package UCRM\Routing
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
interface Middleware
{

    public function run();



}