<?php
declare(strict_types=1);

namespace UCRM\Routing\Middlewares;

/**
 * A class to handle all aspects of a single Route.
 *
 * @package UCRM\Routing
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class BasicAuthenticationMiddleware implements Middleware
{

    public function run()
    {
        // TODO: Implement run() method.
    }
}