<?php
declare(strict_types=1);

namespace UCRM\Routing;

/**
 * A class to handle all aspects of a single Route.
 *
 * @package UCRM\Routing
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class Route implements \JsonSerializable
{
    // -----------------------------------------------------------------------------------------------------------------
    /** @var string[] */
    protected $methods = ["GET"];

    /**
     * @return string[] Returns an array of the supported methods of this route (i.e. ["GET", "POST"]).
     */
    public function getMethods(): array
    {
        return $this->methods;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /** @var string  */
    protected $url = "/";

    /**
     * @return string Returns the URL of this route, including any parameter patterns.
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /** @var array  */
    protected $params = [];

    /**
     * @return array Returns an array of parameters matching the URL pattern of this route.
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params An associative array of parameters, matching the URl pattern of this route.
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /** @var array  */
    protected $query = [];

    /**
     * @return array Returns an associative array of any values passed in the query string.
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array $query An associative array of keys and values from the query string.
     */
    public function setQuery(array $query)
    {
        $this->query = $query;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /** @var callable  */
    protected $handler;

    /**
     * Calls the handler function provided for this Route, passing along the query string and parameters values.
     */
    public function handle(): void
    {
        // TODO: Middleware!


        $handler = $this->handler;
        $handler($this->query, $this->params);
    }











    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Route Constructor.
     *
     * @param string $url The URL pattern to match.
     * @param string[] $methods An array of supported HTTP methods.
     * @param callable $handler A callback function to handle the route when encountered.
     */
    public function __construct(string $url, array $methods, callable $handler)
    {
        $this->url = $url;
        $this->methods = $methods;
        $this->handler = $handler;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @return string Returns a JSON string representation of this Route.
     */
    public function __toString()
    {
        return json_encode($this, JSON_UNESCAPED_SLASHES);
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $assoc = get_object_vars($this);
        unset($assoc["handler"]);

        return $assoc;
    }

}