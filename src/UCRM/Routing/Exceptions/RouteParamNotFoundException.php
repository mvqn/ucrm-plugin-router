<?php
declare(strict_types=1);

namespace UCRM\Routing\Exceptions;

use Exception;



final class RouteParamNotFoundException extends Exception
{
}
