<?php
declare(strict_types=1);

namespace UCRM\Routing\Exceptions;

use Exception;



final class RouterException extends Exception
{
}
