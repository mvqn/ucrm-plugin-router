<?php
declare(strict_types=1);

namespace UCRM\Routing;

/**
 * A class to handle a store a collection of Routes and lookup/pattern matching for those Routes.
 *
 * @package UCRM\Routing
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class RouteMap implements \JsonSerializable
{
    // -----------------------------------------------------------------------------------------------------------------
    /** @const string Defines the character for use in front of parameters in URL patterns. */
    private const PARAMETER_CHAR = ":";

    // -----------------------------------------------------------------------------------------------------------------
    /** @var Route[]  */
    private $_routes = [];

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Adds a Route to this RouteMap.
     *
     * @param string $name The name of the Route, for use in named look-ups.
     * @param Route $route The actual Route object to map.
     * @return RouteMap Returns this RouteMap for use in chaining methods.
     * @throws Exceptions\RouteAlreadyMappedException Throws an exception if the specified Route is already mapped.
     */
    public function add(string $name, Route $route): RouteMap
    {
        // IF the Route name is already in the RouteMap...
        if(array_key_exists($name, $this->_routes))
            // THEN throw an exception!
            throw new Exceptions\RouteAlreadyMappedException("The route '$name' has already been mapped!");

        // OTHERWISE add this Route to the RouteMap collection.
        $this->_routes[$name] = $route;

        // Finally, return THIS RouteMap!
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Deletes a Route from this RouteMap.
     *
     * @param string $name The name of the Route to delete.
     * @return RouteMap Returns this RouteMap for use in chaining methods.
     * @throws Exceptions\RouteNotFoundException Throws an exception if the specified Route is not found.
     */
    public function del(string $name): RouteMap
    {
        // IF the Route name is not in the RouteMap...
        if(!array_key_exists($name, $this->_routes))
            // THEN throw an exception!
            throw new Exceptions\RouteNotFoundException("The route '$name' does not exist!");

        // OTHERWISE delete this Route from the RouteMap collection!
        unset($this->_routes[$name]);

        // Finally, return THIS RouteMap!
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Finds the specified Route, given the name.
     *
     * @param string $name
     * @return Route
     * @throws Exceptions\RouteNotFoundException
     */
    public function findByName(string $name): Route
    {
        // IF the Route name is not in the RouteMap...
        if(!array_key_exists($name, $this->_routes))
            // THEN throw an exception!
            throw new Exceptions\RouteNotFoundException("The route '$name' does not exist!");

        // OTHERWISE return the Route object!
        return $this->_routes[$name];
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Finds the specified Route, given the URL.
     *
     * @param string $url The URL for which to search.
     * @return Route Returns the Route object found.
     * @throws Exceptions\RouteNotFoundException Throws an exception if no Route was found.
     */
    public function findByPath(string $url): Route
    {
        foreach($this->_routes as $route)
        {
            // Get the Route URL pattern.
            $pattern = $route->getUrl();

            // IF the Route URL pattern contains any parameters, as denoted by the specified leading character...
            if(strpos($pattern, self::PARAMETER_CHAR) !== false)
            {
                // THEN explode both the pattern and the actual URL for comparison.
                $p_parts = explode("/", $pattern);
                $u_parts = explode("/", $url);

                // IF the count of comparable parts is not equal...
                if(count($p_parts) !== count($u_parts))
                    // THEN continue, as this cannot be the correct Route!
                    continue;

                // Create a new array to store the assembled pattern.
                $m_parts = [];

                // Create a new array to store the discovered parameters.
                $params = [];

                // Loop through each URL segment...
                for($i = 0; $i < count($p_parts); $i++)
                {
                    // IF the current segment begins with the parameter character...
                    if(strpos($p_parts[$i], self::PARAMETER_CHAR) === 0)
                    {
                        // THEN remove the parameter character and store the key/value pair in the parameters array.
                        $p_name = substr($p_parts[$i], 1, strlen($p_parts[$i]) - 1);
                        $params[$p_name] = $u_parts[$i];
                    }

                    // Include the newly parsed segment in the pattern matching array.
                    $m_parts[] = $p_parts[$i];
                }

                // IF the pattern matching array matches the Route pattern...
                if(implode("/", $m_parts) === $pattern)
                {
                    // THEN set the parameters and return the Route!
                    $route->setParams($params);
                    return $route;
                }
            }
            else
            {
                // OTHERWISE, we simply check to see if there is a literal match and return the Route, if found!
                if($pattern === $url)
                    return $route;
            }
        }

        // No literal or pattern matched Routes were found, so throw an exception!
        throw new Exceptions\RouteNotFoundException("A route for '$url' does not exist!");
    }


    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @return string Returns a JSON string representation of this Route.
     */
    public function __toString()
    {
        return json_encode($this, JSON_UNESCAPED_SLASHES);
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->_routes;
    }

}