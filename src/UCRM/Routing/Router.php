<?php
declare(strict_types=1);

namespace UCRM\Routing;

/**
 * Class Router
 * @package UCRM\Routing
 */
final class Router
{
    // -----------------------------------------------------------------------------------------------------------------
    /** @const string Defines the name of the base file used as the Router. */
    private const ROUTER_FILE = "/public.php";

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Gets the currently requested URI.
     *
     * @return Route Returns the currently requested URI.
     * @throws Exceptions\RouteNotFoundException
     * @throws Exceptions\RouteMethodNotSupportedException
     */
    public function requested(): Route
    {
        // Get the query string if one has been passed, or default to '/'.
        $request = array_key_exists("QUERY_STRING", $_SERVER) ? $_SERVER["QUERY_STRING"] : "/";

        // Split the query string into its individual segments.
        $queries = explode("&", $request);

        // Get the URl from the first segment.
        $url = array_shift($queries);

        // TODO: Sanitize the URL as needed!

        // Determine whether we check for the URL by name or path and do so...
        $route = (strpos($url, "/") !== 0) ? $this->_routeMap->findByName($url): $route = $this->_routeMap->findByPath($url);

        // Get the current request's HTTP method.
        $method = $_SERVER['REQUEST_METHOD'];

        // If the method is not supported by the Route, then throw an exception...
        if(!in_array($method, $route->getMethods()))
            throw new Exceptions\RouteMethodNotSupportedException(
                "The '$method' method is not supported by the '{$route->getUrl()}' route");

        $sanitized = [];
        // Loop through each additional segment in the query string...
        foreach($queries as $query)
        {
            // Construct the key/value pairs.
            list($key, $value) = (strpos($query, "=") !== false) ? array_values(explode("=", $query)) : [$query, ""];

            // TODO: Sanitize the values as needed!

            // Add the pair to te sanitized collection.
            $sanitized[$key] = $value;
        }

        // Add the query string sets to the Route object.
        $route->setQuery($sanitized);

        // Finally, return the Route.
        return $route;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Matches the current request to a Route.
     *
     * @return Route Returns the Route that is responsible for handling the current request.
     * @throws Exceptions\RouteMethodNotSupportedException Throws an exception if the current method is not supported.
     * @throws Exceptions\RouteNotFoundException Throws an exception if no matching Route could be found.
     */
    public function match(): Route
    {
        return $this->requested();
    }

    // -----------------------------------------------------------------------------------------------------------------
    /** @var RouteMap The RouteMap used by this Router. */
    private $_routeMap;

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @return RouteMap Returns the RouteMap used by this Router.
     */
    public function getRouteMap(): RouteMap
    {
        return $this->_routeMap;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Router Constructor.
     *
     * @param RouteMap $routeMap A RouteMap containing all Routes for use by this Router.
     */
    public function __construct(RouteMap $routeMap)
    {
        $this->_routeMap = $routeMap;
    }


    private $_options = [];
    private $_authenticationHandler = null;

    public function useAuthentication(callable $handler)
    {
        $this->_options["authentication"] = true;
        $this->_authenticationHandler = $handler;

    }


}