# ucrm-plugin-core
This library is a helper package for interacting with the UCRM Plugin system.
This package is not a Plugin itself, but will greatly help in the development of actual UCRM Plugins. 

## Installation
Install the latest version with
```bash
$ composer require mvqn/ucrm-plugin-core
```

## Basic Usage
```php
<?php

// COMING SOON
```

## Documentation
COMING SOON

## Third Party Packages
COMING SOON

## About

### Requirements
- This package will be maintained in step with the PHP version used by UCRM to ensure 100% compatibility.
- This package does not require any PHP extensions that are not already enabled in the default UCRM installation.

### Related Packages
[ucrm-plugin-core](https://bitbucket.org/mvqn/ucrm-plugin-rest)\
Another plugin package used to access the UCRM REST API.

[ucrm-plugin-data](https://bitbucket.org/mvqn/ucrm-plugin-data)\
Another plugin package used to access the UCRM database directly.

### Submitting bugs and feature requests
Bugs and feature request are tracked on [Bitbucket](https://bitbucket.org/mvqn/ucrm-plugin-core/issues)

### Author
Ryan Spaeth <[rspaeth@mvqn.net](mailto:rspaeth@mvqn.net)>

### License
Monolog is licensed under the MIT License - see the `LICENSE` file for details.

### Acknowledgements
Credit to the Ubiquiti Team for giving us the luxury of Plugins!