<?php
declare(strict_types=1);

require_once __DIR__."/../../vendor/autoload.php";

use UCRM\Routing\{Router, RouteMap, Route};

/**
 * public.php (optional)
 *
 * If this file is present, a public URL will be generated for the plugin which will point to this file.  When the URL
 * is accessed, the file will be parsed as a PHP script and executed.
 *
 */

$routes = new RouteMap();

$routes->add("test", new Route("/test/:id/:name", ["GET"],
    function(array $query, array $params)
    {
        echo "Hello World!<br/>";
        echo print_r($query, true)."<br/>";
        echo $params["id"]."<br/>";
        echo $params["name"]."<br/>";
    }
));

$routes->add("home", new Route("/", ["GET"],
    function(array $query, array $params)
    {
        echo "Hello World!<br/>";
        echo print_r($query, true)."<br/>";
    }
));

$credentials = [
    "usr"
];

$router = new Router($routes);
$router->useAuthentication(
    function(string $username, string $password)
    {

    }
);


$route = $router->match();
$route->handle();

echo $routes."<br/>";







