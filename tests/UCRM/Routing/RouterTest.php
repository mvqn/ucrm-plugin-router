<?php
declare(strict_types=1);

require_once __DIR__."/../../../vendor/autoload.php";

use UCRM\Routing\Router;
use PHPUnit\Framework\TestCase;




class RouterTest extends TestCase
{



    public function testIsCLI()
    {
        echo (Router::isCLI() ? "CLI" : "HTTP")."\n";
    }




    public function testIsDevelopment()
    {
        echo (Router::isDevelopment() ? "DEV" : "PROD")."\n";

    }








}
